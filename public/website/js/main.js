$(function(){
	$(window).click(function() {
		$(".more-btn-elem .dropdown-menu").hide();
	});

	$('.more-btn-dropdown').click(function(event){
	    event.stopPropagation();
	});

	$("#main-search-input").focusin(function(){
		$("#main-search-btn").fadeIn("fast");
		// $("#main-search-popover").fadeIn("fast");
	});
	$("#main-search-input").focusout(function(){
		$("#main-search-btn").fadeOut("fast");
		$("#main-search-input").val("");
		// $("#main-search-popover").fadeOut("fast");
	});

	$(".global-add-btn").hover(function(){
		$(".global-add-menu").show();
	});

	$(".global-add-dropdown").mouseleave(function(){
		$(".global-add-menu").hide();
	});

	$(".more-btn-elem .more-btn").hover(function(){
		$(".more-btn-elem .dropdown-menu").show();
	});

	$(".more-btn-dropdown").mouseleave(function(){
		$(".more-btn-elem .dropdown-menu").hide();
	});
	
	$(".tab-close-btn").click(function(event){
		var link = $(this).attr("href");
		console.log(link);
		event.stopPropagation();
	});

	$(".global-tabs .tab").click(function(){
		var link = $(this).data("href");
		console.log(link);
	});

	$(window).resize(function() {
		var width = $(window).width();

		if (width > 1180) {

		}
	});
});