<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INSPINIA | Dashboard v.5</title>
    <link href="website/css/bootstrap.min.css" rel="stylesheet">
    <link href="website/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="website/css/animate.css" rel="stylesheet">
    <link href="website/css/base.css?v=<?=time()?>" rel="stylesheet">
    <link href="website/css/style.css?v=<?=time()?>" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <span><img alt="image" class="" src="website/img/logo.svg" width="130" /></span>
                        </div>
                        <div class="logo-element">
                            <img alt="image" class="" src="website/img/logo.svg" width="50" />
                        </div>
                    </li>
                    <li>
                        <a href="layouts.html"><i class="fa fa-tachometer"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                </ul>   
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                    <ul class="nav navbar-top-links navbar-left">
                        <li class="dropdown global-add-dropdown">
                            <a class="dropdown-toggle btn-default btn global-add-btn" data-toggle="dropdown" href="#">+ Add</a>
                            <ul class="dropdown-menu global-add-menu">
                                <li>
                                    <a href="profile.html">
                                        <div>
                                            Job
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="profile.html">
                                        <div>
                                            Client
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="nav navbar-top-links navbar-right">
                        <li class="">
                            <form class="form-inline main-search">
                                <div class="form-group">
                                    <div class="input-group">
                                        <i class="fa fa-search icon"></i>
                                        <input type="text" placeholder="Search job, client, etc..." class="form-control" name="search" id="main-search-input">

                                        <a href="#" class="btn-primary btn-sm btn" id="main-search-btn">Search</a>

                                        <div class="popover bottom" id="main-search-popover" role="tooltip">
                                            <div class="arrow"></div>
                                            <div class="popover-content">
                                                <a href="#">Advanced Search</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-user"></i> John Doe
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="profile.html">
                                        <div class="text-center">
                                            Profile
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="profile.html">
                                        <div class="text-center">
                                            Logout
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>

            <div class="global-tabs row border-bottom white-bg">
                <div class="tabs"></div>
                <ul class="nav more-btn-elem">
                    <li class="dropdown more-btn-dropdown">
                        <a class="dropdown-toggle more-btn" data-toggle="dropdown" href="#"><i class="fa fa-angle-down"></i>More</a>
                        <div class="dropdown-menu">
                        </div>
                    </li>
                </ul>
            </div>

            <div style="display: none" class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2></h2>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper wrapper-content animated fadeInUp">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="m-b-md">
                                            <h2>ABC Wood Working Corp</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <dl class="dl-horizontal">
                                            <dt>Address Line 1:</dt> <dd>181 Village Green Square</dd>
                                            <dt>Address Line 2:</dt> <dd>Suite 1818</dd>
                                            <dt>City:</dt> <dd>Toronto</dd>
                                            <dt>State:</dt> <dd>Ontario</dd>
                                            <dt>Country:</dt> <dd>Canada</dd>
                                        </dl>
                                    </div>
                                    <div class="col-lg-7" id="cluster_info">
                                        <dl class="dl-horizontal">
                                            <dt>Phone:</dt> <dd>514-652-5704</dd>
                                            <dt>Fax:</dt> <dd>-</dd>
                                            <dt>Website:</dt> <dd> <a href="#" class="text-navy">http://abc-woodworking.com</a></dd>
                                        </dl>
                                    </div>
                                </div>
                                <div class="row m-t-sm">
                                    <div class="col-lg-12">
                                    <div class="panel blank-panel">
                                    <div class="panel-heading">
                                        <div class="panel-options">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a href="#tab-1" data-toggle="tab" aria-expanded="true">Jobs</a></li>
                                                <li class=""><a href="#tab-2" data-toggle="tab" aria-expanded="false">Contacts</a></li>
                                                <li class=""><a href="#tab-3" data-toggle="tab" aria-expanded="false">Activities</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane contacts active" id="tab-1">
                                                <button type="button" class="add btn btn-primary pull-right">Add Job</button>

                                                <div class="feed-activity-list">
                                                    <div class="feed-element job">
                                                        <div class="media-body">
                                                            <div class="icon left">
                                                                <i class="fa fa-calendar-o"></i> 
                                                                <div class="date">
                                                                    <label class="month">Aug</label>
                                                                    <label class="day">12</label>
                                                                </div>
                                                                <div class="right icon">
                                                                    <i class="fa fa-check-circle"></i>
                                                                    <label class="plain-bg"></label>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <h3>Full Cleanup</h3>
                                                                <div>
                                                                    Scheduled on <strong>12th April</strong> at <strong>10 am</strong></strong>
                                                                </div>
                                                                <div>Estimated time: <strong>3 hours</strong></div>
                                                                <div class="employees">
                                                                    <div>
                                                                        <label class="staff-short-name">Haress</label>
                                                                        <label class="staff-short-name">Haress</label>
                                                                        <label class="staff-short-name">Haress</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="feed-element job">
                                                        <div class="media-body">
                                                            <div class="icon left cancelled">
                                                                <i class="fa fa-calendar-o"></i> 
                                                                <div class="date">
                                                                    <label class="month">Aug</label>
                                                                    <label class="day">12</label>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <h3>Full Cleanup</h3>
                                                                <div>
                                                                    Scheduled on <strong>12th April</strong> at <strong>10 am</strong></strong>
                                                                </div>
                                                                <div>Estimated time: <strong>3 hours</strong></div>
                                                                <div class="employees">
                                                                    <div>
                                                                        <label class="staff-short-name">Haress</label>
                                                                        <label class="staff-short-name">Haress</label>
                                                                        <label class="staff-short-name">Haress</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="feed-element job">
                                                        <div class="media-body">
                                                            <div class="icon left">
                                                                <i class="fa fa-calendar-o"></i> 
                                                                <div class="date">
                                                                    <label class="month">Aug</label>
                                                                    <label class="day">12</label>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <h3>Full Cleanup</h3>
                                                                <div>
                                                                    Scheduled on <strong>12th April</strong> at <strong>10 am</strong></strong>
                                                                </div>
                                                                <div>Estimated time: <strong>3 hours</strong></div>
                                                                <div class="employees">
                                                                    <div>
                                                                        <label class="staff-short-name">Haress</label>
                                                                        <label class="staff-short-name">Haress</label>
                                                                        <label class="staff-short-name">Haress</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane contacts" id="tab-2">
                                                <button type="button" class="add btn btn-primary pull-right">Add Contact</button>

                                                <div class="feed-activity-list">
                                                    <div class="feed-element">
                                                        <a href="#" class="pull-left">
                                                            <div class="staff-avatar small">
                                                                <label>HD</label>
                                                            </div>
                                                        </a>
                                                        <div class="media-body">
                                                            <div class="m-b-md">
                                                                <h3>Haress Das</h3>
                                                            </div>
                                                            <dl class="dl-horizontal">
                                                                <dt>Title:</dt> <dd>C.T.O</dd>
                                                                <dt>Phone</dt> <dd>514-652-5704</dd>
                                                                <dt>Mobile:</dt> <dd>111-222-3333</dd>
                                                                <dt>Email:</dt> <dd>haressdas@gmail.com</dd>
                                                                <dt>Comment:</dt> <dd>He is the main guy to call.</dd>
                                                            </dl>
                                                        </div>
                                                    </div>
                                                    <div class="feed-element">
                                                        <a href="#" class="pull-left">
                                                            <div class="staff-avatar small">
                                                                <label>HD</label>
                                                            </div>
                                                        </a>
                                                        <div class="media-body">
                                                            <div class="m-b-md">
                                                                <h3>Haress Das</h3>
                                                            </div>
                                                            <dl class="dl-horizontal">
                                                                <dt>Title:</dt> <dd>C.T.O</dd>
                                                                <dt>Phone</dt> <dd>514-652-5704</dd>
                                                                <dt>Mobile:</dt> <dd>111-222-3333</dd>
                                                                <dt>Email:</dt> <dd>haressdas@gmail.com</dd>
                                                                <dt>Comment:</dt> <dd>He is the main guy to call.</dd>
                                                            </dl>
                                                        </div>
                                                    </div>
                                                    <div class="feed-element">
                                                        <a href="#" class="pull-left">
                                                            <div class="staff-avatar small">
                                                                <label>HD</label>
                                                            </div>
                                                        </a>
                                                        <div class="media-body">
                                                            <div class="m-b-md">
                                                                <h3>Haress Das</h3>
                                                            </div>
                                                            <dl class="dl-horizontal">
                                                                <dt>Title:</dt> <dd>C.T.O</dd>
                                                                <dt>Phone</dt> <dd>514-652-5704</dd>
                                                                <dt>Mobile:</dt> <dd>111-222-3333</dd>
                                                                <dt>Email:</dt> <dd>haressdas@gmail.com</dd>
                                                                <dt>Comment:</dt> <dd>He is the main guy to call.</dd>
                                                            </dl>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab-3">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>Status</th>
                                                        <th>Title</th>
                                                        <th>Start Time</th>
                                                        <th>End Time</th>
                                                        <th>Comments</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-primary"><i class="fa fa-check"></i> Completed</span>
                                                        </td>
                                                        <td>
                                                           Create project in webapp
                                                        </td>
                                                        <td>
                                                           12.07.2014 10:10:1
                                                        </td>
                                                        <td>
                                                            14.07.2014 10:16:36
                                                        </td>
                                                        <td>
                                                        <p class="small">
                                                            Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable.
                                                        </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-primary"><i class="fa fa-check"></i> Accepted</span>
                                                        </td>
                                                        <td>
                                                            Various versions
                                                        </td>
                                                        <td>
                                                            12.07.2014 10:10:1
                                                        </td>
                                                        <td>
                                                            14.07.2014 10:16:36
                                                        </td>
                                                        <td>
                                                            <p class="small">
                                                                Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-primary"><i class="fa fa-check"></i> Sent</span>
                                                        </td>
                                                        <td>
                                                            There are many variations
                                                        </td>
                                                        <td>
                                                            12.07.2014 10:10:1
                                                        </td>
                                                        <td>
                                                            14.07.2014 10:16:36
                                                        </td>
                                                        <td>
                                                            <p class="small">
                                                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which
                                                            </p>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-primary"><i class="fa fa-check"></i> Reported</span>
                                                        </td>
                                                        <td>
                                                            Latin words
                                                        </td>
                                                        <td>
                                                            12.07.2014 10:10:1
                                                        </td>
                                                        <td>
                                                            14.07.2014 10:16:36
                                                        </td>
                                                        <td>
                                                            <p class="small">
                                                                Latin words, combined with a handful of model sentence structures
                                                            </p>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-primary"><i class="fa fa-check"></i> Accepted</span>
                                                        </td>
                                                        <td>
                                                            The generated Lorem
                                                        </td>
                                                        <td>
                                                            12.07.2014 10:10:1
                                                        </td>
                                                        <td>
                                                            14.07.2014 10:16:36
                                                        </td>
                                                        <td>
                                                            <p class="small">
                                                                The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
                                                            </p>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-primary"><i class="fa fa-check"></i> Sent</span>
                                                        </td>
                                                        <td>
                                                            The first line
                                                        </td>
                                                        <td>
                                                            12.07.2014 10:10:1
                                                        </td>
                                                        <td>
                                                            14.07.2014 10:16:36
                                                        </td>
                                                        <td>
                                                            <p class="small">
                                                                The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                                                            </p>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-primary"><i class="fa fa-check"></i> Reported</span>
                                                        </td>
                                                        <td>
                                                            The standard chunk
                                                        </td>
                                                        <td>
                                                            12.07.2014 10:10:1
                                                        </td>
                                                        <td>
                                                            14.07.2014 10:16:36
                                                        </td>
                                                        <td>
                                                            <p class="small">
                                                                The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.
                                                            </p>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-primary"><i class="fa fa-check"></i> Completed</span>
                                                        </td>
                                                        <td>
                                                            Lorem Ipsum is that
                                                        </td>
                                                        <td>
                                                            12.07.2014 10:10:1
                                                        </td>
                                                        <td>
                                                            14.07.2014 10:16:36
                                                        </td>
                                                        <td>
                                                            <p class="small">
                                                                Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable.
                                                            </p>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-primary"><i class="fa fa-check"></i> Sent</span>
                                                        </td>
                                                        <td>
                                                            Contrary to popular
                                                        </td>
                                                        <td>
                                                            12.07.2014 10:10:1
                                                        </td>
                                                        <td>
                                                            14.07.2014 10:16:36
                                                        </td>
                                                        <td>
                                                            <p class="small">
                                                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2017
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="website/js/jquery-2.1.1.js"></script>
    <script src="website/js/bootstrap.min.js"></script>
    <script src="website/js/mustache.min.js"></script>
    <script src="website/js/main.js?v=<?=time()?>"></script>

    <script>
    $(function() {
        $(document).on('click', '.dropdown-menu', function (e) {
            e.stopPropagation();
        });

        // Add search row link
        $("#global-table tr").click(function(){
            link = $(this).data("href");
            console.log(link);
        });


        // Tab logic
        // Tab object manager
        Tabs = {
            lastID : 0,
            tabs : [],
            add : function(params) {
                var tab = new Tab(params);
                tab.init(this.lastID);
                tab.setParams(params);
                this.tabs[this.lastID] = tab;
                this.lastID++;
                tab.render();

                
            },
            remove : function(id) {
                var newTabs = [];
                this.tabs.forEach(function (tab, key) {
                    if (key != id) {
                        newTabs[key] = tab;
                    }
                });

                this.tabs = newTabs;

                // Delete current tab
                $("#tab-" + id).remove();

                // Move tab from more if we have space available
                var globalTabsWidth = $(".global-tabs").outerWidth();
                var tabsWidth = $(".global-tabs .tabs").outerWidth();
                var moreBtnWidth = $(".more-btn-elem").outerWidth();
                var tabWidth = $(".global-tabs .tab.main").outerWidth();

                if ((tabsWidth + tabWidth + moreBtnWidth) < globalTabsWidth) {
                    // We have space!
                    // Move the first item on the list to last in tab bar
                    // 1) Get the first item on list
                    $(".global-tabs .dropdown-menu .tab.main").first().appendTo(".global-tabs .tabs");
                }

                // Are there any item left in more?
                if ($(".global-tabs .dropdown-menu .tab.main").length === 0) {
                    $(".global-tabs .more-btn-elem").removeClass("visible");
                }
            }
        };

        // Tab class
        function Tab() {
            this.id = "";
            this.icon = "";

            this.init = function(id) {
                this.id = id;
            };

            this.setParams = function(params) {
                for (var prop in params) {
                    this[prop] = params[prop];
                }

                if (typeof(params.type) != "undefined" && params.type !== null) {
                    if (params.type == "user") {
                        this.icon = '<i class="fa fa-user"></i>';
                    }
                }
            };

            this.render = function() {
                var output = Mustache.render($("#main-tab-tmpl").html(), this);

                var globalTabsWidth = $(".global-tabs").outerWidth();
                var tabsWidth = $(".global-tabs .tabs").outerWidth();
                var moreBtnWidth = $(".more-btn-elem").outerWidth();
                var tabWidth = $(".global-tabs .tab.main").outerWidth();

                if (tabsWidth + tabWidth + moreBtnWidth < globalTabsWidth) {
                    // Good, we have space in tab bar!
                    $(".global-tabs .tabs").prepend(output);
                }
                else {
                    // No more space, add it under more.
                    // Move the last one in tab bar to more
                    $(".global-tabs .tabs .tab.main").last().prependTo(".global-tabs .dropdown-menu");

                    // Make more menu visible
                    $(".global-tabs .more-btn-elem").addClass("visible");
                    $(".global-tabs .tabs").prepend(output);
                }
            };
        };
        
        addTab();

        $(window).resize(function() {
            resizeTimer(function() {
                resizeTab();
            }, 10, "resize");
        });
    });
    
    function resizeTab() {
        console.log("Resizing attempt...");
        
        var globalTabsWidth = $(".global-tabs").outerWidth();
        var tabsWidth = $(".global-tabs .tabs").outerWidth();
        var moreBtnWidth = $(".more-btn-elem").outerWidth();
        var tabWidth = $(".global-tabs .tab.main").outerWidth();

        // var remainingWidth
        console.log("Global : " + globalTabsWidth);
        console.log("Individual components: " + (tabsWidth + tabWidth + moreBtnWidth));
        console.log("Tabs width: " + (tabsWidth));

        // Do we have space to add tab to tab tab?
        if (globalTabsWidth > (tabsWidth + tabWidth + moreBtnWidth + 50)) {

            globalTabsWidth = $(".global-tabs").outerWidth();
            tabsWidth = $(".global-tabs .tabs").outerWidth();
            moreBtnWidth = $(".more-btn-elem").outerWidth();
            tabWidth = $(".global-tabs .tab.main").outerWidth();

            console.log('resizing loop START');
            console.log("Global : " + globalTabsWidth);
            console.log("Individual components: " + (tabsWidth + tabWidth + moreBtnWidth));
            console.log('resizing loop END');

            // Do we have any tab/s in more section?
            if ($(".global-tabs .dropdown-menu .tab.main").length != 0) {
                // Yup, we do!
                $(".global-tabs .dropdown-menu .tab.main").first().appendTo(".global-tabs .tabs");
            }
            
            if ($(".global-tabs .tabs .tab.main").length > 1) {
                // Run next loop
                resizeTimer(function() {
                    resizeTab();
                }, 15, "resize");
            }
        }
        else {
            // Do we have more tabs visible and size is smaller?
            var globalTabsWidth = $(".global-tabs").outerWidth();
            var totalVisibleTabs = $(".global-tabs .tabs .tab.main").length;
            var tabWidth = $(".global-tabs .tabs .tab.main").outerWidth();
            var moreBtnWidth = $(".more-btn-elem").outerWidth();

            if ((totalVisibleTabs * tabWidth + moreBtnWidth + 50) > globalTabsWidth) {
                // Reduce visible tabs, screen is shrinking!
                $(".global-tabs .tabs .tab.main").last().prependTo(".global-tabs .dropdown-menu");

                // Run next loop
                resizeTimer(function() {
                    resizeTab();
                }, 15, "resize");
            }

        }

        // Are there any item left in more?
        if ($(".global-tabs .dropdown-menu .tab.main").length === 0) {
            $(".global-tabs .more-btn-elem").removeClass("visible");
        }
        else {
            $(".global-tabs .more-btn-elem").addClass("visible");   
        }
    }

    var resizeTimer = (function () {
        var timers = {};
        return function (callback, ms, uniqueID) {
            if (!uniqueID) {
                uniqueID = "resize";
            }
            
            if (timers[uniqueID]) {
                clearTimeout(timers[uniqueID]);
            }

            timers[uniqueID] = setTimeout(callback, ms);
        };
    })();

    function addTab() {
        Tabs.add({
            'type' : 'user',
            'line1' : 'Haress Das',
            'line2' : '#1234',
            'url' : 'user/1234',
        });
    }

    function closeTab(id) {
        Tabs.remove(id);
    }
    </script>

    <script id="main-tab-tmpl" type="text/html">
    <div data-href="{{url}}" class="tab main" id="tab-{{id}}">
        <div class="icon">{{{icon}}}</div>
        <div class="content">
            <p>{{line1}} {{id}}</p>
            <p>{{line2}}</p>
        </div>
        <div class="close-container">
            <a href="javascript:void();" onclick="closeTab({{id}});return false;" class="tab-close-btn"><i class="fa fa-times"></i></a>
        </div>
    </div>
    </script>
</body>
</html>