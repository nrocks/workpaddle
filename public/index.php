<?php
date_default_timezone_set('America/Toronto');
// Yii related variables
$yii 		= dirname(__FILE__) . '/../framework/yii.php';
$config 		= dirname(__FILE__) . '/../app/config/';
$domain 		= strtolower($_SERVER['SERVER_NAME']);
$requestUri 	= strtolower($_SERVER['REQUEST_URI']);

if (stripos($domain, 'workpaddle-website.dev') !== false) {
	// Development URLs
	$config .= 'dev/website.php';
}
else if (stripos($domain, 'workpaddle-base.dev') !== false) {
	$config .= 'dev/base.php';
}
else {
	exit;
}

require_once($yii);
Yii::createWebApplication($config)->run();