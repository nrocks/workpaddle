<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INSPINIA | Dashboard v.5</title>
    <link href="website/css/bootstrap.min.css" rel="stylesheet">
    <link href="website/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="website/css/animate.css" rel="stylesheet">
    <link href="website/css/base.css?v=<?=time()?>" rel="stylesheet">
    <link href="website/css/style.css?v=<?=time()?>" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <span><img alt="image" class="" src="website/img/logo.svg" width="130" /></span>
                        </div>
                        <div class="logo-element">
                            <img alt="image" class="" src="website/img/logo.svg" width="50" />
                        </div>
                    </li>
                    <li>
                        <a href="layouts.html"><i class="fa fa-tachometer"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                </ul>   
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                    <ul class="nav navbar-top-links navbar-left">
                        <li class="dropdown global-add-dropdown">
                            <a class="dropdown-toggle btn-default btn global-add-btn" data-toggle="dropdown" href="#">+ Add</a>
                            <ul class="dropdown-menu global-add-menu">
                                <li>
                                    <a href="profile.html">
                                        <div>
                                            Job
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="profile.html">
                                        <div>
                                            Client
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="nav navbar-top-links navbar-right">
                        <li class="">
                            <form class="form-inline main-search">
                                <div class="form-group">
                                    <div class="input-group">
                                        <i class="fa fa-search icon"></i>
                                        <input type="text" placeholder="Search job, client, etc..." class="form-control" name="search" id="main-search-input">

                                        <a href="#" class="btn-primary btn-sm btn" id="main-search-btn">Search</a>

                                        <div class="popover bottom" id="main-search-popover" role="tooltip">
                                            <div class="arrow"></div>
                                            <div class="popover-content">
                                                <a href="#">Advanced Search</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-user"></i> John Doe
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="profile.html">
                                        <div class="text-center">
                                            Profile
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="profile.html">
                                        <div class="text-center">
                                            Logout
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>

            <div class="global-tabs row border-bottom white-bg">
                <div class="tabs"></div>
                <ul class="nav more-btn-elem">
                    <li class="dropdown more-btn-dropdown">
                        <a class="dropdown-toggle more-btn" data-toggle="dropdown" href="#"><i class="fa fa-angle-down"></i>More</a>
                        <div class="dropdown-menu">
                        </div>
                    </li>
                </ul>
            </div>

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Clients</h2>
                    <!-- <a class="btn btn-primary" href="javascript:void(0);" onclick="addTab();">Add</a> -->
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper wrapper-content animated fadeInUp">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="project-list">
                                    <table class="table table-hover" id="global-table">
                                        <tbody>
                                        <tr data-href="1">
                                            <td class="project-title">
                                                <span class="title">ABC Wood Working Corp</span>
                                            </td>
                                            <td class="project-title">
                                                12 Edmonds Street
                                                <br>
                                                <small>Suite 1212, New York, NY, 12345</small>
                                            </td>
                                            <td class="project-title">
                                                <i class="fa fa-phone"></i> 123-445-5555
                                            </td>
                                        </tr>
                                        <tr data-href="1">
                                            <td class="project-title">
                                                <span class="title">ABC Wood Working Corp</span>
                                            </td>
                                            <td class="project-title">
                                                1
                                                <br>
                                                <small>asdad</small>
                                            </td>
                                            <td class="project-title">
                                                <i class="fa fa-phone"></i> 123-445-5555
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2017
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="website/js/jquery-2.1.1.js"></script>
    <script src="website/js/bootstrap.min.js"></script>
    <script src="website/js/mustache.min.js"></script>
    <script src="website/js/main.js?v=<?=time()?>"></script>

    <script>
    $(function() {
        $(document).on('click', '.dropdown-menu', function (e) {
            e.stopPropagation();
        });

        // Add search row link
        $("#global-table tr").click(function(){
            link = $(this).data("href");
            console.log(link);
        });


        // Tab logic
        // Tab object manager
        Tabs = {
            lastID : 0,
            tabs : [],
            add : function(params) {
                var tab = new Tab(params);
                tab.init(this.lastID);
                tab.setParams(params);
                this.tabs[this.lastID] = tab;
                this.lastID++;
                tab.render();

                
            },
            remove : function(id) {
                var newTabs = [];
                this.tabs.forEach(function (tab, key) {
                    if (key != id) {
                        newTabs[key] = tab;
                    }
                });

                this.tabs = newTabs;

                // Delete current tab
                $("#tab-" + id).remove();

                // Move tab from more if we have space available
                var globalTabsWidth = $(".global-tabs").outerWidth();
                var tabsWidth = $(".global-tabs .tabs").outerWidth();
                var moreBtnWidth = $(".more-btn-elem").outerWidth();
                var tabWidth = $(".global-tabs .tab.main").outerWidth();

                if ((tabsWidth + tabWidth + moreBtnWidth) < globalTabsWidth) {
                    // We have space!
                    // Move the first item on the list to last in tab bar
                    // 1) Get the first item on list
                    $(".global-tabs .dropdown-menu .tab.main").first().appendTo(".global-tabs .tabs");
                }

                // Are there any item left in more?
                if ($(".global-tabs .dropdown-menu .tab.main").length === 0) {
                    $(".global-tabs .more-btn-elem").removeClass("visible");
                }
            }
        };

        // Tab class
        function Tab() {
            this.id = "";
            this.icon = "";

            this.init = function(id) {
                this.id = id;
            };

            this.setParams = function(params) {
                for (var prop in params) {
                    this[prop] = params[prop];
                }

                if (typeof(params.type) != "undefined" && params.type !== null) {
                    if (params.type == "user") {
                        this.icon = '<i class="fa fa-user"></i>';
                    }
                }
            };

            this.render = function() {
                var output = Mustache.render($("#main-tab-tmpl").html(), this);

                var globalTabsWidth = $(".global-tabs").outerWidth();
                var tabsWidth = $(".global-tabs .tabs").outerWidth();
                var moreBtnWidth = $(".more-btn-elem").outerWidth();
                var tabWidth = $(".global-tabs .tab.main").outerWidth();

                if (tabsWidth + tabWidth + moreBtnWidth < globalTabsWidth) {
                    // Good, we have space in tab bar!
                    $(".global-tabs .tabs").prepend(output);
                }
                else {
                    // No more space, add it under more.
                    // Move the last one in tab bar to more
                    $(".global-tabs .tabs .tab.main").last().prependTo(".global-tabs .dropdown-menu");

                    // Make more menu visible
                    $(".global-tabs .more-btn-elem").addClass("visible");
                    $(".global-tabs .tabs").prepend(output);
                }
            };
        };
        
        addTab();

        $(window).resize(function() {
            resizeTimer(function() {
                resizeTab();
            }, 10, "resize");
        });
    });
    
    function resizeTab() {
        console.log("Resizing attempt...");
        
        var globalTabsWidth = $(".global-tabs").outerWidth();
        var tabsWidth = $(".global-tabs .tabs").outerWidth();
        var moreBtnWidth = $(".more-btn-elem").outerWidth();
        var tabWidth = $(".global-tabs .tab.main").outerWidth();

        // var remainingWidth
        console.log("Global : " + globalTabsWidth);
        console.log("Individual components: " + (tabsWidth + tabWidth + moreBtnWidth));
        console.log("Tabs width: " + (tabsWidth));

        // Do we have space to add tab to tab tab?
        if (globalTabsWidth > (tabsWidth + tabWidth + moreBtnWidth + 50)) {

            globalTabsWidth = $(".global-tabs").outerWidth();
            tabsWidth = $(".global-tabs .tabs").outerWidth();
            moreBtnWidth = $(".more-btn-elem").outerWidth();
            tabWidth = $(".global-tabs .tab.main").outerWidth();

            console.log('resizing loop START');
            console.log("Global : " + globalTabsWidth);
            console.log("Individual components: " + (tabsWidth + tabWidth + moreBtnWidth));
            console.log('resizing loop END');

            // Do we have any tab/s in more section?
            if ($(".global-tabs .dropdown-menu .tab.main").length != 0) {
                // Yup, we do!
                $(".global-tabs .dropdown-menu .tab.main").first().appendTo(".global-tabs .tabs");
            }
            
            if ($(".global-tabs .tabs .tab.main").length > 1) {
                // Run next loop
                resizeTimer(function() {
                    resizeTab();
                }, 15, "resize");
            }
        }
        else {
            // Do we have more tabs visible and size is smaller?
            var globalTabsWidth = $(".global-tabs").outerWidth();
            var totalVisibleTabs = $(".global-tabs .tabs .tab.main").length;
            var tabWidth = $(".global-tabs .tabs .tab.main").outerWidth();
            var moreBtnWidth = $(".more-btn-elem").outerWidth();

            if ((totalVisibleTabs * tabWidth + moreBtnWidth + 50) > globalTabsWidth) {
                // Reduce visible tabs, screen is shrinking!
                $(".global-tabs .tabs .tab.main").last().prependTo(".global-tabs .dropdown-menu");

                // Run next loop
                resizeTimer(function() {
                    resizeTab();
                }, 15, "resize");
            }

        }

        // Are there any item left in more?
        if ($(".global-tabs .dropdown-menu .tab.main").length === 0) {
            $(".global-tabs .more-btn-elem").removeClass("visible");
        }
        else {
            $(".global-tabs .more-btn-elem").addClass("visible");   
        }
    }

    var resizeTimer = (function () {
        var timers = {};
        return function (callback, ms, uniqueID) {
            if (!uniqueID) {
                uniqueID = "resize";
            }
            
            if (timers[uniqueID]) {
                clearTimeout(timers[uniqueID]);
            }

            timers[uniqueID] = setTimeout(callback, ms);
        };
    })();

    function addTab() {
        Tabs.add({
            'type' : 'user',
            'line1' : 'Haress Das',
            'line2' : '#1234',
            'url' : 'user/1234',
        });
    }

    function closeTab(id) {
        Tabs.remove(id);
    }
    </script>

    <script id="main-tab-tmpl" type="text/html">
    <div data-href="{{url}}" class="tab main" id="tab-{{id}}">
        <div class="icon">{{{icon}}}</div>
        <div class="content">
            <p>{{line1}} {{id}}</p>
            <p>{{line2}}</p>
        </div>
        <div class="close-container">
            <a href="javascript:void();" onclick="closeTab({{id}});return false;" class="tab-close-btn"><i class="fa fa-times"></i></a>
        </div>
    </div>
    </script>
</body>
</html>