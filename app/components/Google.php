<?php
class Google {
	public static function getProfile($token = false) {
		$profile = false;
		$error = false;

		try {
			if ($token) {
				$url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' . $token;
				$curl = curl_init();

				curl_setopt_array($curl, [
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $url,
				]);
				
				$res = curl_exec($curl);

				if ($res = curl_exec($curl)) {
					$res = json_decode($res);

					if (is_object($res)) {
						if (isset($res->sub) && $res->sub) {
							$profile['id'] = $res->sub;
							$profile['email'] = $res->email;
							$profile['name'] = $res->name;
						}
						else if (isset($res->error_description) && $res->error_description) {
							$error = $res->error_description;
						}
					}
				}
				else {
					$error = curl_error($curl);
				}

				curl_close($curl);
			}
		}
		catch (Exception $e) {
			$error = 'Invalid token.';
		}

		if (!$profile) {
			$error = 'Invalid token.';	
		}

		return [$profile, $error];
	}
}