<?php
// Using Amazon Simple Email Services
require(dirname(__FILE__) . '/Composer/vendor/autoload.php');
// use GuzzleHttp\Psr7\Request;
class SnsClient {
	public static function getMessage() {
		$message = false;
		// Make sure the request is POST
		if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
		    http_response_code(405);
		    die;
		}
		 
		try {
		    // Create a message from the post data and validate it's signature
		    $message = \Aws\Sns\Message::fromRawPostData();
		    $validator = new \Aws\Sns\MessageValidator;
		    $validator->validate($message);
		} catch (Exception $e) {
		    // Pretend we're not here if the message is invalid
		    // http_response_code(404);
		    // die;
		}

		if ($message->offsetGet('Type') === 'SubscriptionConfirmation') {
		    // Send a request to the SubscribeURL to complete subscription
			$subscribeURL = $message->offsetGet('SubscribeURL');

		} elseif ($message->offsetGet('Type') === 'Notification') {
			
		}
		return $message;
	}
}