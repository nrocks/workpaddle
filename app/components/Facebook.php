<?php
require(dirname(__FILE__) . '/Composer/vendor/autoload.php');

class Facebook {
	public static function getProfile($token = false) {
		$error = false;
		$profile = false;
		try {
			$fb = new \Facebook\Facebook([
				'app_id' => '',
				'app_secret' => '',
				'default_graph_version' => 'v2.6',
			]);

			// Get the \Facebook\GraphNodes\GraphUser object for the current user.
			// If you provided a 'default_access_token', the '{access-token}' is optional.
			$response = $fb ->get('/me?fields=name,email', $token);

			if (is_object($response)) {
				$profile = $response->getGraphUser();	
			}

		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
			$error = 'Invalid token.';
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			$error = 'Invalid token.';
			// Email::notifyAdmin('Facebook Login Error', 'Facebook SDK returned an error: ' . $e->getMessage());
		}
		catch (Exception $e) {
			$error = 'Invalid token.';
			// Something else happened
			// Email::notifyAdmin('Facebook Login Error', $e->getMessage());
		}

		return [$profile, $error];
	}

	public static function getUserID($token = false) {
		$error = false;
		$userID = false;

		try {
			$fb = new \Facebook\Facebook([
				'app_id' => '',
				'app_secret' => '',
				'default_graph_version' => 'v2.6',
			]);

			// Get the \Facebook\GraphNodes\GraphUser object for the current user.
			// If you provided a 'default_access_token', the '{access-token}' is optional.
			$response = $fb ->get('/me', $token);

			if (is_object($response)) {
				$profile = $response->getGraphUser();	

				$userID = $profile->getField('id');
			}

		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
			$error = 'Invalid token.';
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			$error = 'Invalid token.';
			// Email::notifyAdmin('Facebook Login Error', 'Facebook SDK returned an error: ' . $e->getMessage());
		}
		catch (Exception $e) {
			$error = 'Invalid token.';
			// Something else happened
			// Email::notifyAdmin('Facebook Login Error', $e->getMessage());
		}
		
		return [$userID, $error];
	}
}