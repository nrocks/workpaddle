<?php
require(dirname(__FILE__) . '/Composer/vendor/autoload.php');

class Stripe {
	public static function addCustomer($token = false) {
		$error = false;
		$customer = false;
		try {
			if ($token) {
				\Stripe\Stripe::setApiKey(Yii::app()->params['stripeSecretKey']);
				$customer = \Stripe\Customer::create(array(
					"source" => $token
				));
			}
		} catch (\Stripe\Error\Card $e) {
			$body = $e->getJsonBody();
			$error  = isset($body['error']['message']) ? $body['error']['message'] : 'An error occurred while processing the card.';
		} catch (Exception $e) {
			$error = $e->getMessage();
			// Something else happened
			Email::notifyAdmin('Stripe Add Customer Error', $error);
		}

		return [$customer, $error];
	}

	public static function updateCustomer($customerID = false, $token = false) {
		$error = false;
		$customer = false;
		try {
			if ($customerID && $token) {
				\Stripe\Stripe::setApiKey(Yii::app()->params['stripeSecretKey']);
				$customer = \Stripe\Customer::retrieve($customerID);
				if ($customer) {
					$customer->source = $token;
					$customer->save();
				}
			}
		} catch (\Stripe\Error\Card $e) {
			$body = $e->getJsonBody();
			$error  = isset($body['error']['message']) ? $body['error']['message'] : 'An error occurred while processing the card.';
		} catch (Exception $e) {
			$error = $e->getMessage();
			// Something else happened
			Email::notifyAdmin('Stripe Add Customer Error', $error);
		}

		return [$customer, $error];
	}

	public static function createCharge($customerID, $amount, $description) {
		$error = false;
		$charge = false;
		try {
			if ($customerID) {
				\Stripe\Stripe::setApiKey(Yii::app()->params['stripeSecretKey']);
				$charge = \Stripe\Charge::create(array(
					'amount' => $amount * 100,
					'currency' => 'USD',
					'customer' => $customerID,
					'description' => $description
				));
			}
		} catch (\Stripe\Error\Card $e) {
			$body = $e->getJsonBody();
			$error  = isset($body['error']['message']) ? $body['error']['message'] : 'An error occurred while processing the card.';
		} catch (Exception $e) {
			$error = $e->getMessage();
			// Something else happened
			Email::notifyAdmin('Stripe createCharge Error', $error);
		}

		return $charge;
	}

	public static function getCustomer($customerID) {
		$error = false;
		$customer = false;
		try {
			if ($customerID) {
				\Stripe\Stripe::setApiKey(Yii::app()->params['stripeSecretKey']);
				$customer  = \Stripe\Customer::retrieve($customerID);
			}
		} catch (\Stripe\Error\Card $e) {
			$body = $e->getJsonBody();
			$error  = isset($body['error']['message']) ? $body['error']['message'] : 'An error occurred while processing the card.';
		} catch (Exception $e) {
			$error = $e->getMessage();
			// Something else happened
			Email::notifyAdmin('Stripe getCharges Error', $error);
		}

		return $customer;
	}

	public static function getCharge($chargeID) {
		$error = false;
		$charge = false;
		try {
			if ($chargeID) {
				\Stripe\Stripe::setApiKey(Yii::app()->params['stripeSecretKey']);
				$charge = \Stripe\Charge::retrieve($chargeID);
			}
		} catch (\Stripe\Error\Card $e) {
			$body = $e->getJsonBody();
			$error  = isset($body['error']['message']) ? $body['error']['message'] : 'An error occurred while processing the card.';
		} catch (Exception $e) {
			$error = $e->getMessage();
			// Something else happened
			Email::notifyAdmin('Stripe getCharge Error', $error);
		}

		return $charge;
	}

	public static function getCharges($customerID) {
		$error = false;
		$charges = false;
		try {
			if ($customerID) {
				\Stripe\Stripe::setApiKey(Yii::app()->params['stripeSecretKey']);
				$charges = \Stripe\Charge::all([
					'customer' => $customerID,
					'limit' => 100,
				]);
			}
		} catch (\Stripe\Error\Card $e) {
			$body = $e->getJsonBody();
			$error  = isset($body['error']['message']) ? $body['error']['message'] : 'An error occurred while processing the card.';
		} catch (Exception $e) {
			$error = $e->getMessage();
			// Something else happened
			Email::notifyAdmin('Stripe getCharges Error', $error);
		}

		return $charges;
	}

	public static function getInvoice($invoiceID) {
		$error = false;
		$invoice = false;
		try {
			if ($invoiceID) {
				\Stripe\Stripe::setApiKey(Yii::app()->params['stripeSecretKey']);
				$invoice = \Stripe\Invoice::retrieve($invoiceID);
				
			}
		} catch (\Stripe\Error\Card $e) {
			$body = $e->getJsonBody();
			$error  = isset($body['error']['message']) ? $body['error']['message'] : 'An error occurred while processing the card.';
		} catch (Exception $e) {
			$error = $e->getMessage();
			// Something else happened
			Email::notifyAdmin('Stripe getInvoice Error', $error);
		}

		return $invoice;
	}

	public static function addTestCard() {
		\Stripe\Stripe::setApiKey(Yii::app()->params['stripeSecretKey']);

		$token = \Stripe\Token::create(array(
			"card" => array(
			"number" => "4242424242424242",
			"exp_month" => 1,
			"exp_year" => 2020,
			"cvc" => "505"
			)
		));

		return $token;
	}

	public static function refund($chargeID) {
		$error = false;
		$refund = false;
		try {
			if ($chargeID) {
				\Stripe\Stripe::setApiKey(Yii::app()->params['stripeSecretKey']);
				$refund = \Stripe\Refund::create(['charge' => $chargeID]);
			}
		} catch (\Stripe\Error\Card $e) {
			$body = $e->getJsonBody();
			$error  = isset($body['error']['message']) ? $body['error']['message'] : 'An error occurred while processing the card.';
		} catch (Exception $e) {
			$error = $e->getMessage();
			// Something else happened
			Email::notifyAdmin('Stripe getCharge Error', $error);
		}

		return $refund;
	}
}