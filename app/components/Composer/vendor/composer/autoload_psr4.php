<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twilio\\' => array($vendorDir . '/twilio/sdk/Twilio'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Stripe\\' => array($vendorDir . '/stripe/stripe-php/lib'),
    'Facebook\\' => array($vendorDir . '/facebook/php-sdk-v4/src/Facebook'),
);
