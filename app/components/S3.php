<?php
// Using Amazon Simple Email Services
require(dirname(__FILE__) . '/Composer/vendor/autoload.php');

class S3 {
	public static function uploadFile($fileName = '', $sourceFile) {
		$result = false;
		$error = false;
		
		try {
			// Instantiate the S3 client with your AWS credentials and desired AWS region
			$client = \Aws\S3\S3Client::factory(array(
				'credentials' => array(
					'key'    => Yii::app()->params['awsKey'],
					'secret' => Yii::app()->params['awsSecret'],
	    			),
			    'region' => 'us-east-1',
			    'version' => '2006-03-01'
			));
			
			if ($client) {
				$result = $client->putObject(array(
					'Bucket'     => Yii::app()->params['s3Bucket'],
					'Key'        => $fileName,
					'SourceFile' => $sourceFile,
					'ACL'        => 'public-read'
				));

				$client->waitUntil('ObjectExists', array(
					'Bucket' => Yii::app()->params['s3Bucket'],
					'Key'    => $fileName,
				));
			}
		} catch (\Aws\S3\Exception\S3Exception $e) {
			// Catch an S3 specific exception.
			$error = $e->getMessage();
		} catch (\Aws\Exception\AwsException $e) {
			// This catches the more generic AwsException. You can grab information
			// from the exception using methods of the exception object.
			$error = $e->getAwsErrorType() . $e->getAwsErrorCode();
		}

		return [$result, $error];
	}
}