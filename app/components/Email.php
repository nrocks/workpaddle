<?php
// Using Amazon Simple Email Services
require(dirname(__FILE__) . '/Composer/vendor/autoload.php');
class Email {
	public static function send($to = '', $subject = '', $body = '') {
		if ($to) {
			// Send to common test account for test emails
			if (stripos($to, 'haress') !== false || stripos($to, 'test')  !== false) {
				$to = 'haressdas@gmail.com';
			}

			// Remove when going live
			$to = 'haressdas@gmail.com';

			try {
				$client = \Aws\Ses\SesClient::factory(array(
					'credentials' => array(
						'key'    => Yii::app()->params['awsKey'],
						'secret' => Yii::app()->params['awsSecret'],
					),
					'region' => 'us-east-1',
					'version' => '2010-12-01'
				));

				if ($client) {
					$emailArgs = array(
						'Source' => Yii::app()->params['autoSenderEmail'],
						'Destination' => array(
							'ToAddresses' => array($to), 
							'BccAddresses' => array(Yii::app()->params['adminEmail'])),
						'Message' => array(
							'Subject' => array(
								'Data' => $subject,
							),
							'Body' => array(
								// 'Text' => array(
								// 	'Data' => $textBody,
								// ),
								'Html' => array(
									'Data' => $body,
								),
							),
						),
					);
					$client->sendEmail($emailArgs);
				}
			} catch (Exception $e) {
				var_dump($e->getMessage());
				exit;
			}
		}
	}

	public static function notifyAdmin($subject, $body) {
		self::send(Yii::app()->params['adminEmail'], $subject, $body);
	}
}