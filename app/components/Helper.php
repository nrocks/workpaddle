<?php
class Helper
{
	const STATUS_ACTIVE = 'active';
	const STATUS_INACTIVE = 'inactive';

	public static function goHome()
	{
		Yii::app()->getController()->redirect(array('site/index'));
	}

	public static function exception($error) {
		throw new CHttpException($error);
	}

	public static function getClientIP() {
		$clientIP = '';
		
		if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else if (isset($_SERVER['REMOTE_ADDR'])) {
			$clientIP = $_SERVER['REMOTE_ADDR'];
		}
		
		return $clientIP;
	}

	public static function trimSpaces(&$data = array()) {
		if (is_array($data)) {
			foreach ($data as $key => $value) {
				$data[$key] = trim($value);
			}
		}
	}

	public static function formatCurrency($value, $includeSymbol = true) {
		$value = is_float($value) ? $value : 0;
		$value = number_format($value, 2);

		if ($includeSymbol) {
			$value = '$' . $value;
		}

		return $value;
	}

	public static function pretify($text) {
		return ucwords(strtolower($text));
	}

	public static function forceHTTPS() {
		if (Yii::app()->params['isProd']) {

		if ((isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&  $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'http') || (!isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'http')) {
				header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
				exit();
			}
		}
	}

	public static function forceWWW() {
		if (Yii::app()->params['isProd']) {
			if ((stripos($_SERVER['HTTP_HOST'], 'www.') === false)) {
				header('HTTP/1.1 301 Moved Permanently'); 
				header('Location: ' . $_SERVER['HTTP_X_FORWARDED_PROTO'] . '://www.' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
				exit();
			}
		}
	}

	public static function flattenArray($array = array(), $join = ' ') {
		$array = array_filter($array);
		$array = implode($join, $array);
		return $array;
	}

	public static function generateSalt($length = 12, $allowedChar = false) {
		$characters = '0123456789abcdefghijkl!@#$%^&*()mnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		
		if ($allowedChar == 'number') {
			$characters = '0123456789';
		}
		else if ($allowedChar == 'alphanumeric') {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}
		else if ($allowedChar == 'inviteCode') {
			$characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
		}
		
		$charactersLength = strlen($characters);
		$salt = '';
		for ($i = 0; $i < $length; $i++) {
			$salt .= $characters[rand(0, $charactersLength - 1)];
		}
		return $salt;
	}

	public static function ServicesPubNubAutoload($className) {
		if (strpos($className, 'Pubnub') !== false) {
			$className = ltrim($className, '\\');
			$fileName  = '';

			if ($lastNsPos = strrpos($className, '\\')) {
				$namespace = substr($className, 0, $lastNsPos);
				$className = substr($className, $lastNsPos + 1);
				$fileName .= str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
			}

			$fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
			$fileName = dirname(__FILE__) . DIRECTORY_SEPARATOR . $fileName;

			if (file_exists($fileName)){
				require_once $fileName;
			}
		}
	}

	public static function formatNumber($number = '') {
		$number = preg_replace('/[^0-9]/', '', $number);
		$length = strlen($number);
		$formattedNumber = '';
		for ($i = 0; $i < $length; $i++) {
			if ($i == 3 || $i == 6) {
				$formattedNumber .= '-';
			}
			$formattedNumber .= substr($number, $i, 1);
			
		}
		return $formattedNumber;
	}

	public static function getS3Url($fileName = '') {
		return Yii::app()->params['s3Base'] . Yii::app()->params['s3Bucket'] . '/' . $fileName;
	}

	public static function formatStripeAmount($amount) {
		return '$' . number_format((float)$amount / 100, 2, '.', '');
	}

	public static function setLanguage() {
	}

	public static function blockLoggedInUser($goHome = true) {
		if (isset(Yii::app()->user->id) && Yii::app()->user->id) {
			if ($goHome) {
				$this->redirect(['site/index']);
			}
			else {
				return true;	
			}
		}

		return false;
	}
}