<?php

class SiteController extends CController
{
	public $layout = 'main';
	
	public function actionIndex()
	{
		Yii::app()->end();
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		exit;
		$errorMsg = false;
		if ($error = Yii::app()->errorHandler->error)
		{

		}

	 	$this->render('error', array('errorMsg' => $errorMsg));
	}

	public function actionFlushDb()
	{
		if (Yii::app()->cache->flush())
			echo 'Flushed!';
		else
			echo 'Unable to flush :(';

		exit;
	}
}