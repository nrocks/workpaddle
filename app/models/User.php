<?php
class User extends CActiveRecord
{
	const STATUS_ACTIVE  = 'active';
	const STATUS_INACTIVE = 'deleted';

	public $password;
	public $confirmPassword;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			// Registration scenario
			[
				'firstName, lastName, companyName, email, password, confirmPassword', 
				'required',
				'on' => 'register'],
			['email', 'email', 'on' => 'register'],
			['email', 'validateEmail', 'on' => 'register'],
			['password', 'validatePassword', 'on' => 'register'],

			// Safe fields

			// Search fields
			['id, publicID, storeID, userTypeID, name, status, workStatus, email, hashedPassword, isVirgin, addedDate', 'safe', 'on'=>'search'],
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'publicID' => 'Public',
			'storeID' => 'Store',
			'userTypeID' => 'User Type',
			'name' => 'Name',
			'status' => 'Status',
			'workStatus' => 'Work Status',
			'email' => 'Email',
			'hashedPassword' => 'Hashed Password',
			'isVirgin' => 'Is Virgin',
			'addedDate' => 'Added Date',
		);
	}

	public function getName() {
		return $this->firstName . ' ' . $this->lastName;
	}
	public function validateEmail($attribute) {
		$existingUser = User::model()->find('email = :email AND status = :status', [
			':email' => $this->email,
			':status' => User::STATUS_ACTIVE

		]);

		if ($existingUser) {
			$this->addError($attribute, 'Email is already in use. Please <a href="#">login here</a>.');
		}
	}

	public function validatePassword() {
		// Required
		$validator = CValidator::createValidator('required', $this, ['password', 'confirmPassword']);
		$validator->skipOnError = true;
		$validator->validate($this);

		// Must be between 6 - 12 characters
		$validator = CValidator::createValidator('length', $this, ['password']);
		$validator->min = 6;
		$validator->max = 64;
		$validator->skipOnError = true;
		$validator->validate($this);

		// Compare password and confirmPassword
		$validator = CValidator::createValidator('compare', $this, ['password']);
		$validator->skipOnError = true;
		$validator->compareAttribute = 'confirmPassword';
		$validator->validate($this);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('publicID',$this->publicID);
		$criteria->compare('storeID',$this->storeID);
		$criteria->compare('userTypeID',$this->userTypeID);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('workStatus',$this->workStatus,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('hashedPassword',$this->hashedPassword,true);
		$criteria->compare('isVirgin',$this->isVirgin);
		$criteria->compare('addedDate',$this->addedDate);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	public function verifyPassword($password, $autoLogin = false)
	{
		if ($autoLogin === true) {
			return true;
		}

		return CPasswordHelper::verifyPassword($password, $this->hashedPassword);
	}

	/**
	 * Generates the password hash.
	 * @param string password
	 * @return string hash
	 */
	public function hashPassword($password)
	{
		return CPasswordHelper::hashPassword($password);
	}

	public function beforeSave() {
		if (parent::beforeSave()) {
			if ($this->isNewRecord) {
				if ($this->password) {
					$this->hashedPassword = $this->hashPassword($this->password);
				}

				$this->generatePublicID();
				$this->addedDate = time();
				$this->ipAddress = Helper::getClientIP();
				$this->status = self::STATUS_ACTIVE;
			}
			else {
				if ($this->password && !$this->hasErrors('password')) {
					$this->hashedPassword = $this->hashPassword($this->password);
				}
			}
			return true;
		}
	}

	public function generatePublicID() {
		$isGenerated = false;
		while (!$isGenerated) {
			$this->publicID = md5(microtime() . Helper::generateSalt());
			
			// Let's make sure this is not taken by any one else.
			$user = User::model()->find('publicID = :publicID', [
				':publicID' => $this->publicID
			]);
			if (!$user) {
				$isGenerated = true;
			}
		}
	}
}