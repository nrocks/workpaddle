<?php
$moduleName = 'website';
$cdnBase = '';

$rules = [
	'' =>  $moduleName . '/site/index',
	'login' =>  $moduleName . '/site/login',
	'logout' =>  $moduleName . '/site/logout',
	'register' =>  $moduleName . '/site/register',
	'<controller:\w+>' => $moduleName . '/<controller>/index',
	'<controller:\w+>/<action:\w+>' => $moduleName . '/<controller>/<action>',
	'<controller:\w+>/<action:\w+>/*' => $moduleName . '/<controller>/<action>',
];

$params = [
	'siteName' 			=> '',
	'isProd' 				=> false,
	'source' 				=> 'website',
	'env' 				=> 'dev',
	'websiteBase' 			=> 'http://schedule-website.dev/codehd/InstaLabs/code/schedule/public/',
	'adminEmail' 			=> 'haressdas@gmail.com',
	'autoSenderEmail' 		=> 'noreply@instalabsinc.com',
	's3Bucket' 			=> 'instalabs_schedule',
	'awsKey' 				=> '',
	'awsSecret' 			=> '',
	's3Base' 				=> 'https://s3.amazonaws.com/',
	'stripeSecretKey'		=> '',
	'fbAppID'				=> '',
	'fbsecret'			=> '',
	'cdnBase' 			=> $cdnBase
];

$params = CMap::mergeArray(
	$params,
	require(dirname(__FILE__) . '/devCache.php'));

return [
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR . '../..',
	'name' => '',
	'params' => $params,
	'preload' => ['log'],

	'import' => [
		'application.models.*',
		'application.components.*',
	],
	'defaultController' => 'site',
	'components' => [
		'clientScript' => [
			'packages' => [
				'general' => [
					'baseUrl' => 'website/js/',
					'js' => [],
					'depends' => ['jquery']
		    		],
			],
		],
		'user' => [
			'allowAutoLogin'=>true,
			'class' => 'CWebUser',
			'loginUrl' => ['website/site/login'],
			'stateKeyPrefix' => 'website_',
		],
		'db' => [
			'connectionString' => 'mysql:host=127.0.0.1;dbname=schedule',
			'emulatePrepare' => true,
			'schemaCachingDuration' => 0,
			'username' => 'hd',
			'password' => '**humble**',
			'tablePrefix' => '',	
			'charset' => 'utf8mb4',
		],
		'errorHandler' => [
			'errorAction' => 'website/site/error',
		],
		'urlManager' => [
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'caseSensitive'=>false,
			'rules'=> $rules,
		],
		'log'=>[
			'class'=>'CLogRouter',
			'routes'=>[
				[
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				],
				// uncomment the following to show log messages on web pages
				[
					'class'=>'CWebLogRoute',
				],
			],
		],
		'session' => array (
			'class' => 'CDbHttpSession',
			'connectionID' => 'db',
			'sessionTableName' => 'session',
		),
	],
	'modules'=>[
		'website',
    ],
];