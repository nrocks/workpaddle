<?php
$cdnBase = '';
return [
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR . '../..',
	'name' => 'Main',
	'params' => [
		'siteName'=>'Main',
	],
	'preload' => ['log'],

	'import' => [
		'application.models.versions.' . $versionFolder . '.*',
		'application.components.versions.' . $versionFolder . '.*',
	],
	'defaultController' => 'site',
	'components' => [
		'db' => [
			'connectionString' => 'mysql:host=localhost;dbname=schedule',
			'emulatePrepare' => true,
			'schemaCachingDuration' => 3600,
			'username' => 'hd',
			'password' => '**humble**',
			'tablePrefix' => '',
			'charset' => 'utf8',
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'urlManager' => [
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'caseSensitive'=>false,
			'rules'=>[
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			],
		],
		'log'=>[
			'class'=>'CLogRouter',
			'routes'=>[
				[
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				],
				// uncomment the following to show log messages on web pages
				[
					'class'=>'CWebLogRoute',
				],
			],
		],
	],
	'modules'=>[
		'gii'=>[
	        'class'=>'system.gii.GiiModule',
	        'password'=>'password',
    		],
    ],
];