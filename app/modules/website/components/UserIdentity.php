<?php
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	const ERROR_INACTIVE_ACCOUNT = 3;

	/**
	 * Authenticates an user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate($autoLogin = false) {
		$user = User::model()->find('LOWER(email)=?',array(strtolower($this->username)));
		if ($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if (!$user->verifyPassword($this->password, $autoLogin))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else if ($user->status !== User::STATUS_ACTIVE) {
			$this->errorCode=self::ERROR_INACTIVE_ACCOUNT;
		}
		else {
			$this->_id = $user->id;
			$this->username = $user->email;
			$this->setState('fullName', $user->name);

			// Add the user roles
			// $role = isset($user->role->name) ? $user->role->name : '';
			// $this->setState('role', $role);
			// $this->setState('isVirgin', $user->isVirgin);

			$this->errorCode = self::ERROR_NONE;
		}

		return $this->errorCode == self::ERROR_NONE;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}