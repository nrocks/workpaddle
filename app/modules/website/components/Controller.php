<?php
class Controller extends CController {
	public $layout = 'main';
	public $menu = array();
	public $breadcrumbs = array();

	public function goHome() {
		$this->redirect(array('site/index'));
	}

	public function denied() {
		$this->redirect(array('site/denied'));
	}

	public function isOn($controller = '', $action = '', $returnLinkActive = true) {
		if ($controller == $this->id && $action == $this->action->id) {
			if ($returnLinkActive)
				return 'active';
			else
				return true;
		}	
		else {
			if ($returnLinkActive)
				return '';
			else
				return false;
		}
	}
}