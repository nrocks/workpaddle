<?php
class WebsiteModule extends CWebModule
{
	public function init() {
		$this->setImport(array(
			'website.models.*',
			'website.components.*',
		));
	}

	public function beforeControllerAction($controller, $action) {
		if (parent::beforeControllerAction($controller, $action)) {
			Helper::forceHTTPS();
			Helper::forceWWW();
			Helper::setLanguage();

			// This method is called before any module controller action is performed
			$route = $controller->id . '/' . strtolower($action->id);

			$publicPages = [
				'site/index',
				'site/login',
				'site/register',
				'site/error',
			];

			$publicControllers = [
				'haress',
			];

			if (	in_array($route, $publicPages) || 
				in_array($controller->id, $publicControllers)) {
				return true;
			}
			else if (Yii::app()->user->isGuest) {
				Yii::app()->user->loginRequired();
			}
			else {
				return true;
			}
		}
		
		throw new CHttpException(404,'The specified request cannot be found.');
	}
}