<?php
class HomeController extends Controller {
	public function actionIndex() {
		$this->layout = 'Home';
		$this->pageTitle = Yii::app()->name;
		$this->render('index', [
		]);
	}
}