<?php
class SiteController extends Controller {
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		$this->layout = 'error';

		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			}
			else {
				$this->render('error', $error);
			}
		}
	}

	public function actionIndex() {
		// $user = new User();
		// echo $user->hashPassword('password');
		// exit;

		// var_dump(CPasswordHelper::verifyPassword('password', '$2y$13$RZmVtGmkfk0LmGFiHaLK5.5cKhPQ0K7WvvT84klAN/9Pncb9nsyHm'));
		// exit;
		$this->layout = 'Home';
		$this->pageTitle = Yii::app()->name;
		$this->render('index', array(
		));
	}

	public function actionLogin() {
		Helper::blockLoggedInUser();

		$this->layout = 'form';
		$model = new LoginForm;
		$model->scenario = 'regular';

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax']==='login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes=$_POST['LoginForm'];
			// Validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				$this->redirect(['home/index']);
			}
		}

		if ($model->hasErrors()) {
			$errors = $model->getErrors();

			if ($errors) {
				foreach ($errors as $error) {
					if (isset($error[0])) {
						Yii::app()->user->setFlash('error', $error[0]);
						break;	
					}
				}
			}
		}
		
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(['site/index']);
	}

	public function actionRegister() {
		$this->pageTitle = 'Sign up for free';
		Helper::blockLoggedInUser();

		$this->layout = 'form';
		$model = new User('register');

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax']==='register-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];

			if ($model->validate()) {
				if ($model->save()) {
					// Great, take user to system!
					$loginForm = new LoginForm('autoLogin');
					$loginForm->username = $model->email;
					
					// Validate user input and redirect to the previous page if valid
					if ($loginForm->validate() && $loginForm->login()) {
						$this->redirect(['home/index']);
					}
				}
			}
		}

		if ($model->hasErrors()) {
			$errors = CHtml::errorSummary($model);

			if ($errors) {
				Yii::app()->user->setFlash('error', $errors);
			}
		}
		
		// display the login form
		$this->render('register', [
			'model' => $model
		]);
	}
}