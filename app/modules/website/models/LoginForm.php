<?php
/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $username;
	public $password;
	public $rememberMe;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array('username', 'required'),

			// First scenario, when an API login post occurs
			array('password', 'required', 'on' => 'regular'),
			// password needs to be authenticated
			array('password', 'authenticate', 'on' => 'regular'),

			// Second scenario where we autoLogin the user after create account
			array('password', 'authenticateAutoLogin', 'on' => 'autoLogin'),

			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'username'=>'Email',
			'rememberMe'=>'Remember me',
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if ($this->username && $this->password)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			if (!$this->_identity->authenticate()) {
				if ($this->_identity->errorCode === UserIdentity::ERROR_INACTIVE_ACCOUNT)
					$this->addError('password','Your account is deleted.');
				else
					$this->addError('password','Incorrect username or password.');
			}
		}
	}

	/**
	 * During autoLogin scenario, we simply don't authenticate the password.
	 * Username is still validated.
	 */
	public function authenticateAutoLogin($attribute, $params) {
		$this->_identity=new UserIdentity($this->username, $this->password);
		if(!$this->_identity->authenticate(true))
			$this->addError('password','Incorrect username or password.');
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login($autoLogin = false) {
		if ($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate($autoLogin);
		}
		if ($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration = $this->rememberMe ? 3600*24*30 : 0; // 30 days
			// $duration = 0
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}
}