<? if (Yii::app()->user->hasFlash('success')) : ?>
<div class="alert alert-success">
  <?=Yii::app()->user->getFlash('success')?>
</div>
<? endif; ?>

<? if (Yii::app()->user->hasFlash('error')) : ?>
<div class="alert alert-danger">
  <?=Yii::app()->user->getFlash('error')?>
</div>
<? endif; ?>