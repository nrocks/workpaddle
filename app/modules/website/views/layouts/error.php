<? $baseUrl = Yii::app()->request->baseUrl . '/website/'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Oops</title>
    <link href="<?=$baseUrl?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$baseUrl?>font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?=$baseUrl?>css/base.css" rel="stylesheet">
    <link href="<?=$baseUrl?>css/style.css?v=<?=Yii::app()->params['cssVersion']?>" rel="stylesheet">
</head>
<body class="gray-bg">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a href="" class="navbar-brand dropdown-toggle border-right">
                <img class="logo" src="<?=$baseUrl?>img/schedule_logo.jpg" width="100">
            </a>
          </div>
        </div>
      </nav>
    <?=$content?>
</body>
</html>