<div class="middle-box text-center loginscreen">
   <div>
   		<h3>Login</h3>
   		<? $this->renderPartial('/layouts/flashMessage'); ?>
		<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'login-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array(
		'class' => 'm-t form-signin',
		'role' => 'form'
		),
		)); ?>
		<div class="form-group">
			<?php echo $form->textField($model,'username', array(
				'class' => 'form-control', 
				'required' => 'required', 
				'autofocus' => 'autofocus', 
				'placeholder' => 'Email'
			)); ?>
		</div>
		<div class="form-group">
			<?php echo $form->passwordField($model,'password', array(
				'class' => 'form-control', 
				'required' => 'required',
				'placeholder' => 'Password',
			)); ?>
		</div>
		<button type="submit" class="btn btn-primary block full-width m-b">Login</button>
		<a href="#"><small>Forgot password?</small></a>
		<p class="text-muted text-center"><small>Do not have an account?</small></p>
		<a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>
		<?php $this->endWidget(); ?>
   </div>
</div>