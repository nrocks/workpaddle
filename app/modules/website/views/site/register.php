<div class="middle-box text-center loginscreen register">
	<h2>Sign up for free</h2>
	<? $this->renderPartial('/layouts/flashMessage'); ?>
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'register-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array(
		'class' => 'm-t form-signin',
		'role' => 'form'
	),
	)); ?>

	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<?php echo $form->textField($model,'firstName', array(
					'class' => 'form-control', 
					'required' => 'required', 
					'autofocus' => 'autofocus', 
					'placeholder' => 'First Name'
				)); ?>
				<?php echo $form->error($model,'firstName'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $form->textField($model,'lastName', array(
					'class' => 'form-control', 
					'required' => 'required',
					'placeholder' => 'Last Name'
				)); ?>
				<?php echo $form->error($model,'lastName'); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->textField($model,'companyName', array(
			'class' => 'form-control', 
			'placeholder' => 'Company Name'
		)); ?>
		<?php echo $form->error($model,'companyName'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->textField($model,'email', array(
			'class' => 'form-control', 
			'required' => 'required',
			'placeholder' => 'Email'
		)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<?php echo $form->passwordField($model,'password', array(
					'class' => 'form-control', 
					'required' => 'required', 
					'autofocus' => 'autofocus', 
					'placeholder' => 'Choose a password'
				)); ?>
				<?php echo $form->error($model,'password'); ?>
			</div>
			<div class="col-md-6">
				<?php echo $form->passwordField($model,'confirmPassword', array(
					'class' => 'form-control', 
					'required' => 'required',
					'placeholder' => 'Confirm password'
				)); ?>
				<?php echo $form->error($model,'confirmPassword'); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="row">
			<div class="col-md-4">
				<button type="submit" class="btn btn-primary block full-width m-b">Get Started</button>
			</div>
			<div class="col-md-8 legal text-muted">
				By clicking the button, you agree to our <a href="#" target="blank">legal policies</a>.
			</div>
		</div>
	</div>

	<div class="form-group have-account">
		<p class="text-muted">Already have an account? <a href="#" target="blank">Login</a></p>
	</div>
	
	<?php $this->endWidget(); ?>
</div>